---
title: Kali Linux Image Overview
description:
icon:
weight:
author: ["gamb1t",]
---

Below is an overview of where you can get Kali Linux that is kept up-to-date when a new platform or system is added. Each entry that has a [Kali docs](/docs/) page available will have their page linked.

{{% notice info %}}
Some platforms, like WSL, do not have enough unique methods of obtaining Kali to warrant their own column. However, no method of obtaining Kali is left off this table.
{{% /notice %}}

| Installations | Virtual Machines | Cloud | Containers | USB | ARM (Single Board Computer) | Mobile (NetHunter) |
|---|---|---|---|---|---|---|
| [Standard](/docs/installation/hard-disk-install/)  | [VMware](/docs/virtualization/install-vmware-guest-vm/)  | [AWS](/docs/cloud/aws/) | [Docker](/docs/containers/using-kali-docker-images/) |  Live boot | [Banana Pi](/docs/arm/banana-pi/)  | [NetHunter Rootless](/docs/nethunter/nethunter-rootless/) |
| [Mac](/docs/installation/hard-disk-install-on-mac/)| [Import VMware](/docs/virtualization/import-premade-vmware/) | [Azure](/docs/cloud/azure/)  | [Podman](/docs/containers/using-kali-podman-images/) |  [Persistence](/docs/usb/usb-persistence/)| [Banana Pro](/docs/arm/banana-pro/)| [NetHunter Lite](/docs/nethunter/#10-nethunter-editions) |
| [Dual-booting Mac](/docs/installation/dual-boot-kali-with-mac/)| [VirtualBox](/docs/virtualization/install-virtualbox-guest-vm/)| [Digital Ocean](/docs/cloud/digitalocean/) | [LXC/LXD](/docs/containers/kalilinux-lxc-images/) |  [Encrypted Persistence](/docs/usb/usb-persistence-encryption/) | [BeagleBone Black](/docs/arm/beaglebone-black/)  | [NetHunter](/docs/nethunter/installing-nethunter/) |
| [Dual-booting Windows](/docs/installation/dual-boot-kali-with-windows/) | [Import VirtualBox](/docs/virtualization/import-premade-virtualbox/)| [Linode](/docs/cloud/linode/)  | [WSL](/docs/wsl/wsl-preparations/)|  | [HP Chromebook](/docs/arm/chromebook-exynos/) | Gemini PDA |
| [Dual-booting Linux](/docs/installation/dual-boot-kali-with-linux/) | [Hyper-V](/docs/virtualization/install-hyper-v-guest-vm/)  | | |  | [Samsung Chromebook 1 / 2](/docs/arm/chromebook-exynos/) | LG V20 International |
| Installing to USB| [Parallels](/docs/virtualization/install-parallels-guest-vm/)| | |  | [Acer Tegra Chromebook](/docs/arm/chromebook-nyan/)| Nexus 10 |
| [BTRFS](/docs/installation/btrfs/)| [UTM](/docs/virtualization/install-utm-guest-vm/)| | |  | [ASUS Chromebook Flip](/docs/arm/chromebook-veyron/)| Nexus 5 / 5X |
| [Over a network](/docs/installation/network-pxe/)  | [QEMU/Libvirt](/docs/virtualization/install-qemu-guest-vm/)| | |  | [CubieBoard2](/docs/arm/cubieboard2/)| Nexus 6 / 6P |
| | [Vagrant](/docs/virtualization/install-vagrant-guest-vm/)  | | |  | [CubieBoard3](/docs/arm/cubietruck/)| Nexus 7 |
| | [Proxmox](/docs/virtualization/install-proxmox-guest-vm/)  | | |  | [CuBox](/docs/arm/cubox/)  | Nexus 9 |
| |  |  |  |  | [Cubox-i4Pro](/docs/arm/cubox-i4pro/)| Nokia 3.1 |
| |  |  |  |  | [Gateworks Newport](/docs/arm/gateworks-newport/) | Nokia 6.1 / 6.1 Plus |
| |  |  |  |  | [Gateworks Ventana](/docs/arm/gateworks-ventana/) | OnePlus 2 |
| |  |  |  |  | [Mini-X](/docs/arm/mini-x/) | OnePlus 3 / 3T |
| |  |  |  |  | [NanoPC-T3 / T4](/docs/arm/nanopc-t/) | OnePlus 6 / 6T |
| |  |  |  |  | [NanoPi NEO Plus2](/docs/arm/nanopi-neo-plus2/) | OnePlus 7 / 7 Pro / 7T / 7T Pro |
| |  |  |  |  | [NanoPi2](/docs/arm/nanopi2/) | OnePlus 8 / 8T / 8 Pro |
| |  |  |  |  | [ODROID-C0 / C1 / C1+](/docs/arm/odroid-c/) | OnePlus Nord |
| |  |  |  |  | [ODROID-C2](/docs/arm/odroid-c2/) | OnePlus One |
| |  |  |  |  | [ODROID-U2 / U3](/docs/arm/odroid-u/) | Samsung Galaxy S6 |
| |  |  |  |  | [ODROID-XU3](/docs/arm/odroid-xu3/) | Samsung Galaxy Tab S4 Wi-Fi / LTE |
| |  |  |  |  | [Pinebook](/docs/arm/pinebook/) | Sony Xperia Z1 |
| |  |  |  |  | [Pinebook Pro](/docs/arm/pinebook-pro/) | TicWatch Pro / Pro 4G/LTE / Pro 2020 |
| |  |  |  |  | [Radxa Zero](/docs/arm/radxa-zero-emmc/) | Xiaomi Mi 9T MIUI 11 |
| |  |  |  |  | [Raspberry Pi 1 (Original)](/docs/arm/raspberry-pi/) | Xiaomi Mi A3 |
| |  |  |  |  | [Raspberry Pi 2 (1.1)](/docs/arm/raspberry-pi-2/) | Xiaomi Pocophone F1 |
| |  |  |  |  | [Raspberry Pi 3](/docs/arm/raspberry-pi-3/) | ZTE Axon 7 |
| |  |  |  |  | [Raspberry Pi 4](/docs/arm/raspberry-pi-4/) | |
| |  |  |  |  | [Raspberry Pi 400](/docs/arm/raspberry-pi-400/) |  |
| |  |  |  |  | [Raspberry Pi Zero](/docs/arm/raspberry-pi-zero/) | |
| |  |  |  |  | [Raspberry Pi Zero 2 W](/docs/arm/raspberry-pi-zero-2-w/) |  |
| |  |  |  |  | [Raspberry Pi Zero W](/docs/arm/raspberry-pi-zero-w/) |  |
| |  |  |  |  | [RIoTboard](/docs/arm/riotboard/) |  |
| |  |  |  |  | [Trimslice](/docs/arm/trimslice/) | |
| |  |  |  |  | [USB Armory MKI](/docs/arm/usb-armory-mki/) | |
| |  |  |  |  | [USB Armory MKII](/docs/arm/usb-armory-mkii/) |  |
| |  |  |  |  | [Utilite Pro](/docs/arm/utilite-pro/) |  |